# 1
SELECT DISTINCT maker, (SELECT COUNT(*) FROM product WHERE maker = p1.maker AND type =  'PC') as pc, 
(SELECT COUNT(*) FROM product WHERE maker = p1.maker AND type =  'Laptop') as laptop, 
(SELECT COUNT(*) FROM product WHERE maker = p1.maker AND type =  'Printer') as printer FROM product p1 ORDER BY maker;
# 2
SELECT maker, AVG(screen) FROM product JOIN laptop ON product.model = laptop.model GROUP BY maker;
# 3
SELECT maker, MAX(price) FROM product JOIN pc ON product.model = pc.model GROUP BY maker;
# 4
SELECT maker, MIN(price) FROM product JOIN pc ON product.model = pc.model GROUP BY maker;
# 5
SELECT speed, AVG(price) FROM product JOIN pc ON product.model = pc.model GROUP BY speed HAVING speed > 600;
# 6
SELECT maker, AVG(hd) FROM product JOIN pc ON product.model = pc.model GROUP BY maker HAVING maker IN (SELECT maker FROM product WHERE type = 'Printer');
# 7
SELECT ship, displacement, numGuns FROM outcomes o JOIN ships s JOIN classes c ON o.ship = s.name AND s.class = c.class WHERE battle = 'Guadalcanal'; 
# 8
SELECT ship, country, numGuns FROM outcomes o JOIN ships s JOIN classes c ON o.ship = s.name AND s.class = c.class WHERE result = 'damaged'; 