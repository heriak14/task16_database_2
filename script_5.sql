# 1
# ?SELECT maker FROM product WHERE type = 'PC' AND NOT EXISTS (SELECT model FROM product WHERE maker = product.maker AND model NOT IN (SELECT model FROM pc));
# 2
#SELECT maker FROM product WHERE EXISTS (SELECT model FROM pc WHERE model = product.model AND speed >= 750);
# 3
#SELECT maker FROM product WHERE EXISTS (SELECT model FROM pc WHERE model = product.model AND speed >= 750)
#AND maker IN (SELECT maker FROM product WHERE EXISTS (SELECT model FROM laptop WHERE model = product.model AND speed >= 750));
# 4
#SELECT DISTINCT maker FROM product WHERE type = 'Printer' AND maker IN (
#SELECT maker FROM product JOIN pc ON product.model = pc.model WHERE speed = (SELECT MAX(speed) FROM pc));
# 5
#SELECT name, launched, displacement FROM ships JOIN classes ON ships.class = classes.class WHERE launched > 1922 AND displacement > 35000;
# 6
SELECT class FROM classes WHERE EXISTS (
SELECT name FROM ships s JOIN outcomes o ON s.name = o.ship OR s.class = o.ship 
WHERE result = 'sunk' AND classes.class = s.class);
# 7
#SELECT DISTINCT maker FROM product p WHERE type = 'Laptop' AND EXISTS (SELECT maker FROM product WHERE type = 'Printer' AND p.maker = maker);
# 8
#SELECT DISTINCT maker FROM product p WHERE type = 'Laptop' AND NOT EXISTS (SELECT maker FROM product WHERE type = 'Printer' AND p.maker = maker);