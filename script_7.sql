# 1
#SELECT model, price FROM printer WHERE price = (SELECT MAX(price) FROM printer);
# 2
#SELECT model, model, speed FROM laptop WHERE speed < (SELECT MIN(speed) FROM pc);
# 3
#SELECT maker, price FROM product JOIN printer ON product.model = printer.model WHERE color = 'y' AND price = (SELECT MIN(price) FROM printer WHERE color = 'y');
# 4
#SELECT maker, COUNT(*) AS model_num FROM product WHERE type = 'PC' GROUP BY maker HAVING model_num >= 2;
# 5
#SELECT maker, AVG(hd) FROM product p1 JOIN pc ON p1.model = pc.model GROUP BY maker HAVING maker IN (SELECT maker FROM product WHERE type = 'Printer');
# 6
#SELECT date, count(*) AS count_london FROM trip JOIN pass_in_trip ON trip.trip_no = pass_in_trip.trip_no WHERE town_from = 'London' GROUP BY date;
# 7
#SELECT date, max(count_moscow) AS moscow_races FROM (
#SELECT date, count(*) AS count_moscow FROM trip JOIN pass_in_trip ON trip.trip_no = pass_in_trip.trip_no WHERE town_to = 'Moscow' GROUP BY date
#) AS trips_to_moscow;
# 8
SELECT country, max(ship_num), launched FROM (
SELECT country, launched, count(*) as ship_num FROM ships s JOIN classes c ON s.class = c.class GROUP BY launched) as year_group; 