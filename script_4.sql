# 1
SELECT DISTINCT maker FROM product WHERE maker NOT IN (SELECT maker FROM product WHERE `type` = 'Laptop');
# 2
SELECT DISTINCT maker FROM product WHERE maker != ALL(SELECT maker FROM product WHERE type = 'Laptop');
# 3
#? SELECT DISTINCT maker, type FROM product WHERE maker != ANY(SELECT maker FROM product WHERE type = 'Laptop');
# 4
SELECT DISTINCT maker FROM product WHERE maker IN (SELECT maker FROM product WHERE `type` = 'Laptop') AND type = 'PC';
# 5
#? SELECT DISTINCT maker FROM product WHERE maker = ALL(SELECT maker FROM product WHERE type = 'PC') AND type = 'Laptop';
# 6
SELECT DISTINCT maker, type FROM product WHERE maker = ANY(SELECT maker FROM product WHERE type = 'Laptop') AND type = 'PC';
# 7
SELECT maker FROM product WHERE model IN (SELECT model from PC);
SELECT maker FROM product WHERE model != ALL(SELECT model from PC);
SELECT maker FROM product WHERE model = ANY (SELECT model from PC);
# 8
SELECT country, class FROM classes WHERE country = ANY(SELECT country FROM classes WHERE country = 'Ukraine') OR TRUE;
# 9
SELECT ship, battle, date FROM outcomes JOIN battles b1 ON outcomes.battle = b1.name 
WHERE result = 'damaged' AND ship IN (SELECT ship FROM outcomes o JOIN battles b ON o.battle = b.name WHERE date > b1.date);
# 10
SELECT COUNT(*) FROM product WHERE maker = 'A' AND type = 'PC';
# 11
SELECT DISTINCT maker FROM product WHERE type = 'PC' AND model NOT IN (SELECT model FROM pc);
# 12
SELECT model, price FROM laptop WHERE price > ALL(SELECT price FROM pc);