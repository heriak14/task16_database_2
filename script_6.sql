# 1
SELECT concat('середня ціна = ', AVG(price)) AS avg_price FROM laptop;
# 2
SELECT concat('Модель:', model, ', частота процесора:', speed, ', об`єм ОП:', ram, ', об`м диску:', hd, ', cd:', cd, ', ціна:', price) AS main_info FROM pc;
# 3
SELECT date_format(date, '%Y.%m.%d') AS date FROM income;
# 4
SELECT ship, battle, CASE WHEN result = 'sunk' THEN 'потоплено' WHEN result = 'damaged' THEN 'пошкоджено' WHEN result = 'OK' THEN 'Все ОК' END AS результат FROM outcomes;
# 5
SELECT trip_no, date, ID_psg, concat('ряд:', substr(place, 1, 1)) AS ряд, concat('місце:', substr(place, 2)) AS місце FROM pass_in_trip;
# 6
SELECT trip_no, ID_comp, plane, concat('from ', town_from, ' to ', town_to) AS route, time_out, time_in FROM trip;