#1
SELECT maker, p1.model, p1.`type`, price FROM product p1 JOIN laptop l ON p1.model = l.model WHERE maker = "B"
UNION SELECT maker, p2.model, p2.`type`, price FROM product p2 JOIN pc ON p2.model = pc.model WHERE maker = "B"
UNION SELECT maker, P3.model, P3.`type`, price FROM product p3 JOIN printer pr ON P3.model = pr.model WHERE maker = "B";
#2
SELECT p1.`type`, p1.model, price FROM product p1 JOIN laptop l ON p1.model = l.model GROUP BY p1.model 
HAVING price = (SELECT MAX(price) FROM product p2 JOIN laptop l2 ON p2.model = l2.model WHERE p2.model = p1.model)
UNION 
SELECT p3.`type`, p3.model, price FROM product P3 JOIN pc pc1 ON p3.model = pc1.model GROUP BY p3.model 
HAVING price = (SELECT MAX(price) FROM product P4 JOIN pc pc2 ON p4.model = pc2.model WHERE P4.model = p3.model)
UNION 
SELECT p5.`type`, p5.model, price FROM product p5 JOIN printer pr1 ON p5.model = pr1.model GROUP BY p5.model 
HAVING price = (SELECT MAX(price) FROM product p6 JOIN printer pr2 ON p6.model = pr2.model WHERE p6.model = p5.model);
#3
SELECT maker,`type`, AVG(price) FROM product p1 JOIN laptop l ON p1.model = l.model WHERE maker = "A"
UNION 
SELECT maker, `type`, AVG(price) FROM product p2 JOIN pc ON P2.model = pc.model WHERE maker = "A";
#8
SELECT name FROM ships WHERE launched < 1942 ;
